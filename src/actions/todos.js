import makeEnum from '../utils/makeEnum';

export const types = makeEnum([
  'TODO_ADD',
  'TODO_EDIT',
  'TODO_REMOVE',
  'TODO_CHECK',
  'TODO_CLEAR_COMPLETED',
]);

export function add({ text }) {
  return {
    type: types.TODO_ADD,
    text,
  };
}

export function edit({ id, text }) {
  return {
    type: types.TODO_EDIT,
    id,
    text,
  };
}

export function remove({ id }) {
  return {
    type: types.TODO_REMOVE,
    id,
  };
}

export function check({ id }) {
  return {
    type: types.TODO_CHECK,
    id,
  };
}

export function clearCompleted() {
  return {
    type: types.TODO_CLEAR_COMPLETED,
  };
}
