export const types = Object.freeze({
  'FILTER_ALL' : 'all',
  'FILTER_CHECKED': 'done',
  'FILTER_UNCHECKED': 'in-progress'
});