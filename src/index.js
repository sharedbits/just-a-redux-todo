import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom'


import registerServiceWorker from './registerServiceWorker';

import AppWrapper from './containers/AppWrapper';

import reducer from './reducers';

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);


const AppRouter = ({ store }) => (
    <Router>
      <Route path="/:filter?" component={AppWrapper} />
    </Router>
)


const appWithStore = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);

ReactDOM.render(appWithStore, document.getElementById('root'));
registerServiceWorker();
