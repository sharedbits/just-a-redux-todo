import React, { Component } from 'react';
import '../App.css';

import Todo from './Todo';
import Nav from './Nav';
 
class App extends Component {
 
  constructor () {
    super();
 
    this.state = {
      editInput : '',
      editId: undefined,
    }
  }

  clearInput = () => {
    this.setState({
      editInput: '',
      editId: undefined,
    });
  }

  insert = () => {
    this.props.todoActions.add({
      text: this.state.editInput,
    });
  }

  update = () => {
    this.props.todoActions.edit({
      id: this.state.editId,
      text: this.state.editInput,
    });
  }

  keyDownHandler = (evt) => {
    if (evt.which === 13) {

      if (this.state.editId) {
        this.update();
      } else {
        this.insert();
      }
      this.clearInput();
    } else if (evt.which === 27) {
      this.clearInput();
    }
  }

  changeHandler = (evt) => {
    this.setState({ editInput: evt.target.value });
  }

  handleEdit = ({ id }) => {
    this.setState({
      editInput: this.props.todos.find((todo) => todo.id === id).text,
      editId: id,
    });
  }

  clearCompleted = () => {
    this.props.todoActions.clearCompleted();
  }
 
  render() {
    const { props } = this;

    const itemsLeft = props.todos.reduce((sum, todo) => sum + (todo.checked ? 0 : 1), 0);

    return (
      <div className="App">
        <input type="test" onKeyDown={this.keyDownHandler} onChange={this.changeHandler} value={this.state.editInput} />
        <div>
          { props.todos.map(todo => (
            <Todo
              {...todo}
              key={todo.idx}
              onCheck={props.todoActions.check}
              onEdit={this.handleEdit}
              onRemove={props.todoActions.remove}
            />
          ))}
        </div>
        <div class="footer">
          Items left {itemsLeft}
          <Nav />
          <button onClick={this.clearCompleted}>Clear completed</button>
        </div>
      </div>
    );
  }
}
 
export default App;