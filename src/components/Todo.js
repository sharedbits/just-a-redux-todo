import React, { Component } from 'react';
import PropTypes from 'prop-types';


export default class Todo extends Component {
  handleCheck = (evt) => {
    this.props.onCheck({ id: this.props.id });
  }

  handleEdit = (evt) => {
    this.props.onEdit({ id: this.props.id });
  }

  handleRemove = (evt) => {
    this.props.onRemove({ id: this.props.id });
  }

  render() {
    const { props } = this;

    return (
      <div className="todo">
        <input type="checkbox" checked={props.checked} onClick={this.handleCheck} />
        <span onDoubleClick={this.handleEdit}>
          {props.text}
        </span>
        <button onClick={this.handleRemove}>
          Delete
        </button>
      </div>
    );
  }
}

Todo.propTypes = {
  id: PropTypes.number.isRequired,
  checked: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  onEdit: PropTypes.func.isRequired,
  onCheck: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
};