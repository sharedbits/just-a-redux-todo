import React from 'react';
import Link from '../containers/Link';
import { types } from '../actions/filters';

const Nav = () => (
  <span>
    {' '}
    Show:{' '} 
    <Link filter={types.FILTER_ALL}>
      All
    </Link>
    {' '}
    <Link filter={types.FILTER_UNCHECKED}>
      Unchecked
    </Link>
    {' '}
    <Link filter={types.FILTER_CHECKED}>
      Done
    </Link>
    {' '}
  </span>
)

export default Nav;