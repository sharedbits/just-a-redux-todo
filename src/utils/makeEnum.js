export default function makeEnum(ar) {
  return Object.freeze(ar.reduce((obj, curr) => ({
    ...obj,
    [curr]: curr
  }), {}));
}
