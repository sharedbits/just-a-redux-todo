import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  add, edit, remove, check, clearCompleted,
} from '../actions/todos';

import {
  types
} from '../actions/filters';

import App from '../components/App';


function filterTodos(filter, todos) {
  switch(filter) {
    case types.FILTER_CHECKED:
      return todos.filter((todo) => todo.checked);
    case types.FILTER_UNCHECKED:
      return todos.filter((todo) => !todo.checked);  
    case types.FILTER_ALL:
    default:
      return todos;
  }
}

const mapStateToProps = (state, ownProps) => ({
  todos: filterTodos(ownProps.match.params.filter, state.todos.list),
});

const mapDispatchToProps = dispatch => ({
  todoActions: bindActionCreators({
    add,
    edit,
    check,
    remove,
    clearCompleted,
  }, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
