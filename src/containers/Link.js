import React from 'react';
import { NavLink } from 'react-router-dom';
import { types } from '../actions/filters';

const Link = ({ filter, children }) => (
  <NavLink
    to={filter === types.FILTER_ALL ? '/' : `/${ filter }`}
  >
    {children}
  </NavLink>
);

export default Link;