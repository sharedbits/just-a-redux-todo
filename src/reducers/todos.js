import { types } from '../actions/todos';

export const initialState = {
  list: [],
  nextId: 1,
};

export default function todos(state = initialState, action) {
  switch (action.type) {
    case types.TODO_ADD:
      return {
        list: [
          ...state.list,
          {
            id: state.nextId,
            text: action.text,
            checked: false,
          },
        ],
        nextId: state.nextId + 1,
      };

    case types.TODO_EDIT:
      return {
        ...state,
        list: state.list.map(todo => (
          todo.id === action.id ? { ...todo, text: action.text } : todo
        )),
      };

    case types.TODO_REMOVE:
      return {
        ...state,
        list: state.list.filter(todo => todo.id !== action.id),
      };

    case types.TODO_CHECK:
      return {
        ...state,
        list: state.list.map(todo => (
          todo.id === action.id ? { ...todo, checked: !todo.checked } : todo
        )),
      };

    case types.TODO_CLEAR_COMPLETED:
      return {
        ...state,
        list: state.list.filter(todo => !todo.checked),
      };

    default:
      return state;
  }
}
